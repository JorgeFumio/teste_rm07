<?php

header("Content-type: text/html; charset=utf-8");
include("conecta.php");

$postdata = file_get_contents("php://input");

$request = json_decode($postdata);

$nome = $request->nome_razao;
$cpf = $request->cpf_cnpj;
$tipo_pessoa = $request->tipo_pessoa;
$tel = $request->telefone;
$cel = $request->celular;
$ehzap = $request->eh_whats;
$obs = $request->observacao;
$eh_cliente = $request->cliente_ou_fornecedor;

$insert = "INSERT INTO pessoas (nome_razao, cpf_cnpj, tipo_pessoa, telefone, celular, eh_whats, observacao, cliente_ou_fornecedor) values (:nome, :cpf, :tipo_pessoa, :tel, :cel, :ehzap, :obs, :eh_cliente)";

try{
	$resultado = $pdo->prepare($insert);
	$resultado->bindParam(':nome',$nome,PDO::PARAM_STR);
	$resultado->bindParam(':cpf',$cpf,PDO::PARAM_STR);
	$resultado->bindParam(':tipo_pessoa',$tipo_pessoa,PDO::PARAM_STR);
	$resultado->bindParam(':tel',$tel,PDO::PARAM_STR);
	$resultado->bindParam(':cel',$cel,PDO::PARAM_STR);
	$resultado->bindParam(':ehzap',$ehzap,PDO::PARAM_STR);
	$resultado->bindParam(':obs',$obs,PDO::PARAM_STR);
	$resultado->bindParam(':eh_cliente',$eh_cliente,PDO::PARAM_STR);

	if($resultado->execute()){
		$select = "SELECT * FROM pessoas ORDER BY id DESC";
		$resultado2 = $pdo->prepare($select);
		// $resultado2->bindParam(':email',$email,PDO::PARAM_STR);
		$resultado2->execute();
		$row = $resultado2->fetchObject();
		$respostaserver = array(
		"status" => "Cadastrado",
		"mensagem" => "Cadastro Realizado com Sucesso!",
		"id" => $row->id,
		"nome_razao" => $row->nome_razao,
		"cpf_cnpj" => $row->cpf_cnpj,
		"tipo_pessoa" => $row->tipo_pessoa,
		"telefone" => $row->telefone,
		"celular" => $row->celular,
		"eh_whats" => $row->eh_whats,
		"observacao" => $row->observacao,
		"cliente_ou_fornecedor" => $row->cliente_ou_fornecedor
		);

	} else {
			$respostaserver = array(
			"status" => "Oops",
			"mensagem" =>"Verifique se todos os campos foram preenchidos!"
			);


	}


}

catch(PDOExeption $e) {


}

exit(json_encode($respostaserver));



?>
