<?php

header("Content-type: text/html; charset=utf-8");
include("conecta.php");

$postdata = file_get_contents("php://input");

$request = json_decode($postdata);

$titulo = $request->titulo;
$desc = $request->desc;
$valor = $request->valor;

$insert = "INSERT INTO servicos (titulo, descricao, valor) values (:titulo, :descricao, :valor);";

try{
	$resultado = $pdo->prepare($insert);
	$resultado->bindParam(':titulo',$titulo,PDO::PARAM_STR);
	$resultado->bindParam(':descricao',$desc,PDO::PARAM_STR);
	$resultado->bindParam(':valor',$valor,PDO::PARAM_STR);
	

	if($resultado->execute()){
		
		$respostaserver = array(
		"status" => "Cadastrado",
		"mensagem" => "Cadastro de servico Realizado com Sucesso!",
		);

	} else {
			$respostaserver = array(
			"status" => "Oops",
			"mensagem" =>"Verifique se todos os campos foram preenchidos!"
			);


	}


}

catch(PDOExeption $e) {


}

exit(json_encode($respostaserver));



?>
