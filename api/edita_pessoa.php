<?php

header("Content-type: text/html; charset=utf-8");
include("conecta.php");

$postdata = file_get_contents("php://input");

$request = json_decode($postdata);

$id = $request->id3;
$nome = $request->nome_razao;
$cpf = $request->cpf_cnpj;
$tipo_pessoa = $request->tipo_pessoa;
$tel = $request->telefone;
$cel = $request->celular;
$ehzap = $request->ehzap2;
$obs = $request->observacao;
$eh_cliente = $request->cliente_ou_fornecedor;

$insert = "UPDATE pessoas set nome_razao = :nome, cpf_cnpj = :cpf, tipo_pessoa = :tipo_pessoa, telefone = :tel, celular = :cel, eh_whats = :ehzap, observacao = :obs, cliente_ou_fornecedor = :eh_cliente WHERE id = :id;";

try{
	$resultado = $pdo->prepare($insert);
	$resultado->bindParam(':id',$id,PDO::PARAM_STR);
	$resultado->bindParam(':nome',$nome,PDO::PARAM_STR);
	$resultado->bindParam(':cpf',$cpf,PDO::PARAM_STR);
	$resultado->bindParam(':tipo_pessoa',$tipo_pessoa,PDO::PARAM_STR);
	$resultado->bindParam(':tel',$tel,PDO::PARAM_STR);
	$resultado->bindParam(':cel',$cel,PDO::PARAM_STR);
	$resultado->bindParam(':ehzap',$ehzap,PDO::PARAM_STR);
	$resultado->bindParam(':obs',$obs,PDO::PARAM_STR);
	$resultado->bindParam(':eh_cliente',$eh_cliente,PDO::PARAM_STR);

	if($resultado->execute()){
	
	$select = "SELECT * FROM pessoas ORDER BY id DESC";
	$resultado2 = $pdo->prepare($select);
	
	
	if($resultado2->execute()){
		$i = 0;
		while($row = $resultado2->fetchObject()){
			if($resultado2->rowCount() > 0) {
			$respostaserver[$i] = array(
			"status" => "ok",
			"mensagem" => "Lista completa!",
			"id" => $row->id,
			"nome_razao" => $row->nome_razao,
			"cpf_cnpj" => $row->cpf_cnpj,
			"tipo_pessoa" => $row->tipo_pessoa,
			"telefone" => $row->telefone,
			"celular" => $row->celular,
			"eh_whats" => $row->eh_whats,
			"observacao" => $row->observacao,
			"cliente_ou_fornecedor" => $row->cliente_ou_fornecedor
			);
			$i++;
		} else {
			$respostaserver[] = array(
				"status" => "Oops",
				'mensagem' => ''
			);
		}
		}
		

	} else {
			$respostaserver[] = array(
			"status" => "Oops",
			"mensagem" =>"Erro ao consultar!"
			);


	}

	} else {
			$respostaserver = array(
			"status" => "Oops",
			"mensagem" =>"Verifique se todos os campos foram preenchidos!"
			);


	}


}

catch(PDOExeption $e) {


}

exit(json_encode($respostaserver));



?>
