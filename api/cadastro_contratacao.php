<?php

header("Content-type: text/html; charset=utf-8");
include("conecta.php");

$postdata = file_get_contents("php://input");

$request = json_decode($postdata);

$pessoa = $request->pessoa_id;
$servico = $request->servico_id;
$now2 = date('Y-m-d');
$lastc = $now2;
$valor = $request->valor;


$insert = "INSERT INTO pessoas_servicos (pessoas_id, servicos_id, data_inicio, ultima_cobranca_em, valor_atual) values (:pessoa, :servico, :now2, :lastc, :valor)";

try{
	$resultado = $pdo->prepare($insert);
	$resultado->bindParam(':pessoa',$pessoa,PDO::PARAM_STR);
	$resultado->bindParam(':servico',$servico,PDO::PARAM_STR);
	$resultado->bindParam(':now2',$now2,PDO::PARAM_STR);
	$resultado->bindParam(':lastc',$lastc,PDO::PARAM_STR);
	$resultado->bindParam(':valor',$valor,PDO::PARAM_STR);
	
	if($resultado->execute()){
		$respostaserver = array(
		"status" => "ok",
		"mensagem" => "Contratação Realizada com Sucesso!",
		);

	} else {
			$respostaserver = array(
			"status" => "Oops",
			"mensagem" =>"Verifique se todos os campos foram preenchidos!"
			);


	}


}

catch(PDOExeption $e) {


}

exit(json_encode($respostaserver));



?>
