<?php

header("Content-type: text/html; charset=utf-8");
include("conecta.php");

try{
	
	$select = "SELECT pessoas_servicos.id, pessoas.id as pessoas_id, pessoas.cpf_cnpj, pessoas.nome_razao, servicos.id as servicos_id, servicos.titulo, servicos.descricao, servicos.valor, pessoas_servicos.valor_atual from pessoas, servicos JOIN pessoas_servicos WHERE (pessoas.id = pessoas_servicos.pessoas_id AND servicos.id = pessoas_servicos.servicos_id) ORDER BY pessoas_servicos.id DESC";
	$resultado2 = $pdo->prepare($select);
	
	
	if($resultado2->execute()){
		$i = 0;
		while($row = $resultado2->fetchObject()){
			if($resultado2->rowCount() > 0) {
			$respostaserver[$i] = array(
			"status" => "ok",
			"mensagem" => "Lista completa!",
			"id" => $row->id,
			"pessoas_id" => $row->pessoas_id,
			"nome_razao" => $row->nome_razao,
			"cpf_cnpj" => $row->cpf_cnpj,
			"servicos_id" => $row->servicos_id,
			"titulo" => $row->titulo,
			"descricao" => $row->descricao,
			"valor" => $row->valor,
			"valor_atual" => $row->valor_atual,
			);
			$i++;
		} else {
			$respostaserver[] = array(
				"status" => "Oops",
				"mensagem" => "Sem Pessoas cadastradas ainda"
			);
		}
		}
		

	} else {
			$respostaserver[] = array(
			"status" => "Oops",
			"mensagem" =>"Erro ao consultar!"
			);


	}


}

catch(PDOExeption $e) {


}

exit(json_encode($respostaserver));



?>
